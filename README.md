# Image Enhanced

Enhanced version of the `Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter`
with support for tokenized Alt, Title and Custom link fields.

## Dependencies

* [Token](https://www.drupal.org/project/token)
* [Media Entity](https://www.drupal.org/project/media_entity)
