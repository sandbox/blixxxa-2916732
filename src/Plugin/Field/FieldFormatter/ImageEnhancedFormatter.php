<?php

namespace Drupal\image_enhanced\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Component\Utility\Xss;

/**
 * Plugin implementation of the 'image_enhanced' formatter.
 *
 * @FieldFormatter(
 *   id = "image_enhanced",
 *   label = @Translation("Enhanced image"),
 *   field_types = {
 *     "image"
 *   },
 *   quickedit = {
 *     "editor" = "image"
 *   }
 * )
 */
class ImageEnhancedFormatter extends ImageFormatter implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'alt_text' => '',
        'title_text' => '',
        'custom_link' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['alt_text'] = [
      '#title' => t('Alternative text'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('alt_text'),
      '#description' => t('This field supports tokens.'),
      '#maxlength' => 255,
    ];
    $element['title_text'] = [
      '#title' => t('Title text'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('title_text'),
      '#description' => t('This field supports tokens.'),
      '#maxlength' => 255,
    ];

    // Add the token tree UI.
    $element['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['media', 'node'],
      '#dialog' => TRUE,
    ];

    return parent::settingsForm($form, $form_state) + $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    
    if (!empty($this->getSetting('alt_text'))) {
      $summary[] = t('Alternative text: @value', ['@value' => $this->getSetting('alt_text')]);
    }

    if (!empty($this->getSetting('title_text'))) {
      $summary[] = t('Title text: @value', ['@value' => $this->getSetting('title_text')]);
    }

    return parent::settingsSummary() + $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    
    $elements = parent::viewElements($items, $langcode);
    
    $entity = $items->getEntity();
    
    foreach ($elements as &$element) {
      $alt_text = \Drupal::token()->replace($this->getSetting('alt_text'), ['media' => $entity, 'node' => $entity], ['clear' => TRUE]);
      $element['#item_attributes']['alt'] = Xss::filter($alt_text, Xss::getHtmlTagList());

      $title_text = \Drupal::token()->replace($this->getSetting('title_text'), ['media' => $entity, 'node' => $entity], ['clear' => TRUE]);
      $element['#item_attributes']['title'] = Xss::filter($title_text, Xss::getHtmlTagList());
    }
    

    return $elements;
  }

}